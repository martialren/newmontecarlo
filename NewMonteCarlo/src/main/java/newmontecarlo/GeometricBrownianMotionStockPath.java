package newmontecarlo;

import montecarlo.*;
import org.joda.time.DateTime;

public class GeometricBrownianMotionStockPath implements StockPath {
	private DateTime begin;
	private int n;//number of steps, can't put a long in an array-length, so it's an integer
	private AntiTheticNormalRandomVectorGenerator ng;
	private long lag;//number of millis per step, it's a long
	private double startprice;
	public GeometricBrownianMotionStockPath(DateTime begin,DateTime end,String volprecision,double vol,String rateprecision,double rate,double startprice){//default one millisecond steps,
		if(end.isBefore(begin)){
			System.out.println("Warning! "+begin.toString()+" is after "+end.toString());
			return;
		}
		this.begin=begin;
		this.n=(int) (end.getMillis()-begin.getMillis());
		//this.ng=new AntiTheticNormalRandomVectorGenerator(n,new NormalizedBoxMullerRandomGenerator());
		this.ng=new AntiTheticNormalRandomVectorGenerator(n,new GPUNormalGenerator());
		this.lag=1;
		buildVolatility(volprecision, vol);
		buildRate(rateprecision,rate);
		this.startprice=startprice;
	}
	public GeometricBrownianMotionStockPath(DateTime begin,DateTime end,String precision,String volprecision,double vol,String rateprecision,double rate,double startprice){//default millisecond steps,
		if(end.isBefore(begin)){
			System.out.println("Warning! "+begin.toString()+" is after "+end.toString());
			return;
		}
		this.begin=begin;
		long millis=end.getMillis()-begin.getMillis();
		this.lag=getLag(precision,1);
		this.n=(int) (millis/lag);
		//this.ng=new AntiTheticNormalRandomVectorGenerator(n,new NormalizedBoxMullerRandomGenerator());
		this.ng=new AntiTheticNormalRandomVectorGenerator(n,new GPUNormalGenerator());
		buildVolatility(volprecision, vol);
		buildRate(rateprecision,rate);
		this.startprice=startprice;
	}
	public GeometricBrownianMotionStockPath(DateTime begin,DateTime end,int num,String volprecision,double vol,String rateprecision,double rate,double startprice){//default millisecond steps,
		if(end.isBefore(begin)){
			System.out.println("Warning! "+begin.toString()+" is after "+end.toString());
			return;
		}
		this.begin=begin;
		long millis=end.getMillis()-begin.getMillis();
		this.lag=getLag("MILLIS",num);
		this.n=(int) (millis/lag);
		//this.ng=new AntiTheticNormalRandomVectorGenerator(n,new NormalizedBoxMullerRandomGenerator());
		this.ng=new AntiTheticNormalRandomVectorGenerator(n,new GPUNormalGenerator());
		buildVolatility(volprecision, vol);
		buildRate(rateprecision,rate);
		this.startprice=startprice;
	}
	public GeometricBrownianMotionStockPath(DateTime begin,DateTime end,String precision, int num,String volprecision,double vol,String rateprecision,double rate,double startprice){//default millisecond steps,
		if(end.isBefore(begin)){
			System.out.println("Warning! "+begin.toString()+" is after "+end.toString());
			return;
		}
		this.begin=begin;
		long millis=end.getMillis()-begin.getMillis();
		this.lag=getLag(precision,num);
		this.n=(int) (millis/lag);
		//this.ng=new AntiTheticNormalRandomVectorGenerator(n,new NormalizedBoxMullerRandomGenerator());
		//this.ng=new AntiTheticNormalRandomVectorGenerator(n,new NormalizedRandomRandomGenerator());
		this.ng=new AntiTheticNormalRandomVectorGenerator(n,new GPUNormalGenerator());
		buildVolatility(volprecision, vol);
		buildRate(rateprecision,rate);
		this.startprice=startprice;
	}
	@Override
	public List<Pair<DateTime, Double>> getPrices() {
		double[] path=ng.getVector();
		double price=this.startprice;
		for(int i=0;i<path.length;i++){
			price*=Math.exp(path[i]*vol+rate-vol*vol/2);
			path[i]=price;
		}
		return new PList(path,begin,lag);
	}
	private long getLag(String s,long n){
		if(s.equals("SECONDS")){
			return 1000*n;
		}
		if(s.equals("MINUTES")){
			return 60000*n;
		}
		if(s.equals("HOURS")){
			return 3600000*n;
		}
		if(s.equals("DAYS")){
			return 86400000*n;
		}
		else{
			return n;//default in MILLIS
		}
	}
	private double getLag(String s,double d){
		if(s.equals("SECONDS")){
			return d/1000;
		}
		if(s.equals("MINUTES")){
			return d/60000;
		}
		if(s.equals("HOURS")){
			return d/3600000;
		}
		if(s.equals("DAYS")){
			return d/86400000;
		}
		else{
			return d;//default in MILLIS
		}
	}	
	private double getLagVol(String s,double d){
		if(s.equals("SECONDS")){
			return d/Math.sqrt(1000);
		}
		if(s.equals("MINUTES")){
			return d/Math.sqrt(60000);
		}
		if(s.equals("HOURS")){
			return d/Math.sqrt(3600000);
		}
		if(s.equals("DAYS")){
			return d/Math.sqrt(86400000);
		}
		else{
			return d;//default in MILLIS
		}
	}
	private void buildVolatility(String precision, double sigma){
		double volMillis=getLagVol(precision,sigma);
		this.vol=volMillis*Math.sqrt(lag);
	}
	private void buildRate(String precision, double r){
		double rateMillis=getLag(precision,r);
		this.rate=rateMillis*lag;
	}
	private double vol;
	private double rate;
}
