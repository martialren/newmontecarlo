package newmontecarlo;

import GPUUniToNormal.GPUUniToNormal;

/**
 * Created by Martial on 10/12/2014.
 */
public class GPUNormalGenerator implements montecarlo.NormalizedRandomGenerator{
    @Override
    public double nextRandom() {
        if(this.i<2*n){
            i++;
            return norm[i-1];
        }
        else{
            float[] a = new float[n];
            float[] b = new float[n];
            for(int i=0;i<n;i++){
                a[i]= (float)Math.random();
                b[i]=(float)Math.random();
            }
            this.norm=g.getNormal(a,b);
            this.i=0;
            i++;
            return norm[i-1];
        }
    }

    final int n= 1024*1024;
    float[] norm;
    GPUUniToNormal g;
    int i;//next to be read, from 0 to 1024*1024

    public GPUNormalGenerator(){
        float[] a = new float[n];
        float[] b = new float[n];
        for(int i=0;i<n;i++){
            a[i]= (float)Math.random();
            b[i]=(float)Math.random();
        }
        this.g=new GPUUniToNormal();
        this.norm=g.getNormal(a,b);
        this.i=0;
    }

    public static void main(String[] args){
        GPUNormalGenerator g = new GPUNormalGenerator();
        for(int i=0;i<1024*1024*2;i++){
            g.nextRandom();
            //System.out.println(g.nextRandom());
        }
    }
}
