package GPUUniToNormal;

/**
 * Created by Martial on 10/12/2014.
 */

import com.nativelibs4java.opencl.*;
import org.bridj.Pointer;

import static org.bridj.Pointer.allocateFloats;

public class GPUUniToNormal {

    public GPUUniToNormal(){
        // Creating the platform which is out computer.
        CLPlatform clPlatform = JavaCL.listPlatforms()[1];
        // Getting the GPU device
        //CLDevice device = clPlatform.listGPUDevices(true)[0];
        CLDevice device = clPlatform.listAllDevices(true)[0];
        // Verifing that we have the GPU device
        //System.out.println("*** New device *** ");
        //System.out.println("Vendor: " + device.getVendor());
        //System.out.println("Name: " + device.getName() );
        //System.out.println("Type: " + device.getType() );
        // Let's make a context
        CLContext context = JavaCL.createContext(null, device);
        // Lets make a default FIFO queue.
        CLQueue queue = context.createDefaultQueue();

        // Read the program sources and compile them :
        String src = "__kernel void boxmullerTrans(__global const float* a, __global const float* b, __global float* out,  int n) \n" +
                "{\n" +
                "    int i = get_global_id(0);\n" +
                "    if (i >= n)\n" +
                "        return;\n" +
                "\n" +
                "    float r=sqrt(-2*log(a[i]));\n"+
                "    float t=2*3.14159*b[i];\n" +
                "    out[2*i] = r*cos(t);\n" +
                "    out[2*i+1] = r*sin(t);\n" +
                "}";
        CLProgram program = context.createProgram(src);
        program.build();

        CLKernel kernel = program.createKernel("boxmullerTrans");



        //update local fields
        this.context=context;
        this.queue=queue;
        this.aPtr=aPtr;
        this.bPtr=bPtr;
        this.kernel=kernel;
    }

    protected CLContext context;
    protected CLQueue queue;
    protected Pointer<Float> aPtr,bPtr;
    protected CLKernel kernel;
    final int n=1024*1024; //can't put 1024*1024 here, it's too large

    public float[] getNormal(float[] aa , float[] bb){
        Pointer<Float>
                aPtr = allocateFloats(n),
                bPtr = allocateFloats(n);

        for (int i = 0; i < n; i++) {
            aPtr.set(i, aa[i]);
            bPtr.set(i, bb[i]);
        }

        // Create OpenCL input buffers (using the native memory pointers aPtr and bPtr) :
        CLBuffer<Float>
                a = context.createFloatBuffer(CLMem.Usage.Input, aPtr),
                b = context.createFloatBuffer(CLMem.Usage.Input, bPtr);

        // Create an OpenCL output buffer :
        CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.Output, 2*n);
        kernel.setArgs(a, b, out,n);

        CLEvent event = kernel.enqueueNDRange(queue, new int[]{n}, new int[]{128});

        Pointer<Float> cPtr = out.read(queue,event);

        float[] normal=new float[2*n];
        for ( int i = 0; i < 2*n; ++i){
            normal[i]=cPtr.get(i);
        }
        return normal;

    }

    public static void main(String[] args){
        GPUUniToNormal g =  new GPUUniToNormal();
        int n=1024*1024;
        g.getNormal(new float[n],new float[n]);
    }
}
