package montecarlo;

public interface PayOut {
	public double getPayOut(StockPath path);
}
