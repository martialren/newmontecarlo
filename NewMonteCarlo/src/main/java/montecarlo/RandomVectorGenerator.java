package montecarlo;

public interface RandomVectorGenerator {
	public double[] getVector();
}
