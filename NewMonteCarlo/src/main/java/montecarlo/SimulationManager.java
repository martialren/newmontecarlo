package montecarlo;

import newmontecarlo.GeometricBrownianMotionStockPath;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.joda.time.DateTime;

public class SimulationManager {

	/**
	 * @param args
	 * @author Martial
	 */
	public static void main(String[] args) {
		double tolerance=0.01;
		launch("EUROCALL",165,"DAYS",252,"DAYS",1,"DAYS",0.01,"DAYS",0.0001,152.35,0.96,tolerance);
		///////////launch(option, strike,maturity,,precision,,volprecisi,,rateprecisi,,startprice,proba,tolerance
		launch("ASIANCALL",164,"DAYS",252,"DAYS",1,"DAYS",0.01,"DAYS",0.0001,152.35,0.96,tolerance);
	}
	
	public static void launch(String option, double strike,String maturityunit,int maturityvalue, String precision, int num,String volprecision,double vol,String rateprecision,double rate,double startprice,double probability,double tolerance){
		DateTime t=DateTime.now();
		DateTime tt;
		if(maturityunit.equals("YEARS")){tt=t.plusYears(maturityvalue);}
		else if(maturityunit.equals("MONTHS")){tt=t.plusMonths(maturityvalue);}
		else if(maturityunit.equals("DAYS")){tt=t.plusDays(maturityvalue);}
		else if(maturityunit.equals("HOURS")){tt=t.plusHours(maturityvalue);}
		else if(maturityunit.equals("MINUTES")){tt=t.plusMinutes(maturityvalue);}
		else if(maturityunit.equals("SECONDS")){tt=t.plusSeconds(maturityvalue);}
		else{tt=t.plus(maturityvalue);}//MILLIS by default
		GeometricBrownianMotionStockPath gb=new GeometricBrownianMotionStockPath(t,tt,precision,num,volprecision,vol,rateprecision,rate,startprice);
		PayOut po;
		if(option.equals("ASIANCALL")){po=new AsianCallPayOut(strike);}
		else {po=new EuroCallPayOut(strike);}//if(option.equals("EUROCALL")), eurocall by default
		//
		MeanStdTracker mst=new MeanStdTracker();
		mst.add(po.getPayOut(gb));
		double s=Double.MAX_VALUE;
		NormalDistribution nd=new NormalDistribution();
		double a=nd.inverseCumulativeProbability((probability+1)/2);
		double aa2=a*a*2;
		double tol2=tolerance*tolerance/aa2;
		double cumultol=tol2;
		while(s>=cumultol&&s>0){//reflushing the variables with some algebra
			mst.add(po.getPayOut(gb));
			s=mst.getStd2();
			cumultol+=tol2;
			System.out.println(mst.getN());
			//System.out.println(mst.getMean());
		}
		System.out.println(mst.getN());
		System.out.println(mst.getMean());
	}
}
