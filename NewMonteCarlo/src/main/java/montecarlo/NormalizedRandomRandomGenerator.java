package montecarlo;

import java.util.Random;

public class NormalizedRandomRandomGenerator implements NormalizedRandomGenerator{
	Random rn;
	public NormalizedRandomRandomGenerator(){
		this.rn=new Random();
	}
	@Override
	public double nextRandom() {
		return rn.nextGaussian();
	}
	
}
