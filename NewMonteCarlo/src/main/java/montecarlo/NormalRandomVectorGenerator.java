package montecarlo;

public class NormalRandomVectorGenerator implements RandomVectorGenerator{
	public int n;
	public NormalizedRandomGenerator ng;
	public NormalRandomVectorGenerator(int n,NormalizedRandomGenerator ng){
		this.n=n;//can't use a long for an array-length anyway
		this.ng=ng;
	}
	@Override
	public double[] getVector() {
		double[] vect=new double[n];
		for(int i=0;i<n;i++){
			vect[i]=ng.nextRandom();
		}
		return vect;
	}
}
