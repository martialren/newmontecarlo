package montecarlo;

public class EuroCallPayOut implements PayOut{
	public EuroCallPayOut(double strike){
		this.strike=strike;
	}
	private double strike;
	@Override
	public double getPayOut(StockPath path) {
		return Math.max(0,((PList)path.getPrices()).getLast().getSecond()-strike);
	}

}
