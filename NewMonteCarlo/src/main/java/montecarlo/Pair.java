package montecarlo;

public class Pair<T1, T2> {
	public T1 t1;
	public T2 t2;
	public Pair(T1 t11,T2 t21){
		this.t1=t11;
		this.t2=t21;
	}
	public T1 getFirst(){
		return t1;
	}
	public T2 getSecond(){
		return t2;
	}
}
