package montecarlo;

import org.joda.time.DateTime;
import montecarlo.Pair;

public class PList implements List<Pair<DateTime, Double>>{
	DateTime current;
	long lag;
	double[] path;
	int i;
	DateTime begin;
	int n;
	public PList(double[] path, DateTime begin, long lag) {
		this.current=begin.plus(lag);
		this.lag=lag;
		this.path=path;
		this.i=0;
		this.n=path.length;
		this.begin=begin;
	}
	public Pair<DateTime,Double> getLast(){
		//DateTime end=begin;
		DateTime end=begin.plus(lag*n);
		return new Pair<DateTime, Double>(end,path[n-1]);
	}
	public boolean hasNext(){
		return i<n;
	}
	public Pair<DateTime,Double> next(){
		Pair<DateTime, Double> p=new Pair<DateTime, Double>(current,path[i]);
		i++;
		this.current=current.plus(lag);
		return p;
	}
	

}
