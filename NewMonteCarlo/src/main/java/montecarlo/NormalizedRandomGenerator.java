package montecarlo;

public interface NormalizedRandomGenerator {
	public double nextRandom();
}
