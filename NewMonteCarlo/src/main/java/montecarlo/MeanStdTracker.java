package montecarlo;

public class MeanStdTracker {
	int num;
	double sum;
	double sum2;
	public MeanStdTracker(){
		//the values are already at zero;
	}
	public void add(double value){
		num++;
		this.sum+=value;
		this.sum2+=value*value;
	}
	public double getMean(){
		return sum/num;
	}
	public double getStd2(){//the variance actually
		return sum2/num-sum*sum/num/num;
	}
	public int getN(){
		return num;
	}
}
