package montecarlo;

public class AntiTheticNormalRandomVectorGenerator implements
		RandomVectorGenerator {//antithetic decorator for normal vector

	private boolean used;
	private double[] vect;
	private NormalRandomVectorGenerator ng;
	private int n;
	public AntiTheticNormalRandomVectorGenerator(int n,NormalizedRandomGenerator nng){
		this.n=n;
		this.ng=new NormalRandomVectorGenerator(n,nng);
		this.used=true;
	}
	public double[] getVector() {
		if(!used){
			this.used=true;
			return this.vect;}
		else{
			this.used=false;
			double[] newVect=ng.getVector();
			this.vect=new double[n];//don't use the old one
			for(int i=0;i<this.n;i++){
				vect[i]=-newVect[i];
			}
			return newVect;
		}
	}

}
