package montecarlo;

public class NormalizedBoxMullerRandomGenerator implements
		NormalizedRandomGenerator {//decorator using the Box-Muller transform
	private double next1;
	private double next2;
	private boolean used1;
	private boolean used2;
	public NormalizedBoxMullerRandomGenerator(){
		this.used1=false;
		this.used2=false;
		double u1=Math.random();
		double u2=Math.random();
		double r=Math.sqrt(-2*Math.log(u1));
		double t=2*Math.PI*u2;
		this.next1=r*Math.cos(t);
		this.next2=r*Math.sin(t);
	}
	@Override
	public double nextRandom() {
		if(!used1){used1=true;return next1;}
		if(!used2){used2=true;return next2;}
		this.used2=false;
		double u1=Math.random();
		double u2=Math.random();
		double r=Math.sqrt(-2*Math.log(u1));
		double t=2*Math.PI*u2;
		this.next1=r*Math.cos(t);
		this.next2=r*Math.sin(t);
		return next1;
	}
	
}
