package montecarlo;

public class AsianCallPayOut implements PayOut{
	public AsianCallPayOut(double strike){
		this.strike=strike;
	}
	private double strike;
	@Override
	public double getPayOut(StockPath path) {
		PList l=(PList)path.getPrices();
		int num=0;
		double d=0;
		while(l.hasNext()){
			num++;
			d+=l.next().getSecond();
		}
		return Math.max(0, d/num-strike);
	}

}
